# 2D Plotting

In Python, the *matplotlib* is the most important package that to make a plot, you can have a look of the [matplotlib gallery](https://matplotlib.org/gallery/index.html#gallery) and get a sense of what could be done there. Usually the first thing we need to do to make a plot is to import the matplotlib package. In Jupyter notebook, we could show the figure directly within the notebook and also have the interactive operations like pan, zoom in/out, and so on using the magic command - *%matplotlib notebook*. Let's see some examples. 

import numpy as np
import matplotlib.pyplot as plt
%matplotlib notebook

The basic plotting function is *plot(x,y)*. The *plot* function takes in two lists/arrays, x and y, and produces a visual display of the respective points in x and y. 

**TRY IT!** Given the lists x = [0, 1, 2, 3] and y = [0, 1, 4, 9], use the plot function to produce a plot of x versus y. 

x = [0, 1, 2, 3] 
y = [0, 1, 4, 9]
plt.plot(x, y)
plt.show()

You will notice in the above figure that by default, the plot function connects each point with a blue line. To make the function look smooth, use a finer discretization points. The *plt.plot* function did the main job to plot the figure, and *plt.show()* is telling Python that we are done plotting and please show the figure. Also, you can see some buttons beneath the plot that you could use it to move the line, zoom in or out, save the figure. Note that, before you plot the next figure, you need to turn off the interactive plot by pressing the *stop interaction* button on the top right of the figure. Otherwise, the next figure will be plotted in the same frame. Or we could simply using the magic function *%matplotlib inline* to turn off the interactive features.  

**TRY IT!** Make a plot of the function $f(x) = x^2 for -5\le x \le 5$. 

%matplotlib inline

x = np.linspace(-5,5, 100)
plt.plot(x, x**2)
plt.show()

To change the marker or line, you can put a third input argument into plot, which is a string that specifies the color and line style to be used in the plot. For example, *plot(x,y,'ro')* will plot the elements of x against the elements of y using red, r, circles, 'o'. The possible specifications are shown below in the table. 


| Symbol  | Description  | Symbol  |  Description |
|:---:|:---:|:---:|:---:|
|  b | blue  |  T |  T |
|  g | green |  s | square  |
|  r |  red |  d |  diamond |
|  c |  cyan |  v | triangle (down)  |
|  m |  magenta | ^  | triangle (up)   |
|  y |  yellow | <  | triangle (left)   |
|  k | black  |  > |  triangle (right)  |
|  w |  white | p  | pentagram  |
|  . |  point |  h |  hexagram |
|  o |  circle | -  |  solid |
|  x | x-mark  |  : |  dotted |
|  + |  plus | -.  | dashdot  |
|  * |  star |  --  | dashed  |

**TRY IT!** Make a plot of the function $f(x) = x^2 for -5\le x \le 5$ using a dashed green line. 

x = np.linspace(-5,5, 100)
plt.plot(x, x**2, 'g--')
plt.show()

Before the *plt.show()* statement, you can add in and plot more datasets within one figure. 

**TRY IT!** Make a plot of the function $f(x) = x^2 and g(x) = x^3 for -5\le x \le 5$. Use different colors and markers for each function. 

x = np.linspace(-5,5,20)
plt.plot(x, x**2, 'ko')
plt.plot(x, x**3, 'r*')
plt.show()

It is customary in engineering and science to always give your plot a title and axis labels so that people know what your plot is about. Besides, sometimes you want to change the size of the figure as well. You can add a title to your plot using the *title* function, which takes as input a string and puts that string as the title of the plot. The functions *xlabel* and *ylabel* work in the same way to name your axis labels. For changing the size of the figure, we could create a figure object and resize it. Note, every time we call *plt.figure* function, we create a new figure object to draw something on it. 

**TRY IT!** Add a title and axis labels to the previous plot. And make the figure larger with width 10 inches, and height 6 inches. 

plt.figure(figsize = (10,6))

x = np.linspace(-5,5,20)
plt.plot(x, x**2, 'ko')
plt.plot(x, x**3, 'r*')
plt.title(f'Plot of Various Polynomials from {x[0]} to {x[-1]}')
plt.xlabel('X axis', fontsize = 18)
plt.ylabel('Y axis', fontsize = 18)
plt.show()

We can see that we could change any part of the figure, such as the x and y axis label size by specify a *fontsize* argument in the *plt.xlabel* function. But there are some pre-defined styles that we could use to automatically change the style. Here is the list of the styles. 

print(plt.style.available)

One of my favorite is the *seaborn* style, we could change it using the *plt.style.use* function, and let's see if we change it to 'seaborn-poster', it will make everything bigger. 

plt.style.use('seaborn-poster')

plt.figure(figsize = (10,6))

x = np.linspace(-5,5,20)
plt.plot(x, x**2, 'ko')
plt.plot(x, x**3, 'r*')
plt.title(f'Plot of Various Polynomials from {x[0]} to {x[-1]}')
plt.xlabel('X axis')
plt.ylabel('Y axis')
plt.show()

You can add a legend to your plot by using the *legend* function. And add a *label* argument in the *plot* function. The legend function also takes argument of *loc* to indicate where to put the legend, try to change it from 0 to 10. 

plt.figure(figsize = (10,6))

x = np.linspace(-5,5,20)
plt.plot(x, x**2, 'ko', label = 'quadratic')
plt.plot(x, x**3, 'r*', label = 'cubic')
plt.title(f'Plot of Various Polynomials from {x[0]} to {x[-1]}')
plt.xlabel('X axis')
plt.ylabel('Y axis')
plt.legend(loc = 2)
plt.show()

Finally, you can further customize the appearance of your plot to change the limits of each axis using the *xlim* or *ylim* function. Also, you can use the *grid* function to turn on the grid of the figure. 

**TRY IT!** Change the limits of the plot so that x is visible from -6 to 6 and y is visible from -10 to 10. Turn the grid on. 

plt.figure(figsize = (10,6))

x = np.linspace(-5,5,100)
plt.plot(x, x**2, 'ko', label = 'quadratic')
plt.plot(x, x**3, 'r*', label = 'cubic')
plt.title(f'Plot of Various Polynomials from {x[0]} to {x[-1]}')
plt.xlabel('X axis')
plt.ylabel('Y axis')
plt.legend(loc = 2)
plt.xlim(-6.6)
plt.ylim(-10,10)
plt.grid()
plt.show()

We can create a table of plots on a single figure using the *subplot* function. The *subplot* function takes three inputs: the number of rows of plots, the number of columns of plots, and to which plot all calls to plotting functions should plot. You can move to a different subplot by calling the subplot again with a different entry for the plot location.

There are several other plotting functions that plot x versus y data. Some of them are *scatter*, *bar*, *loglog*, *semilogx*, and *semilogy*. *scatter* works exactly the same as plot except it defaults to red circles (i.e., *plot(x,y,'ro')* is equivalent to *scatter(x,y)*). The *bar* function plots bars centered at x with height y. The *loglog*, *semilogx*, and *semilogy* functions plot the data in x and y with the x and y axis on a log scale, the x axis on a log scale and the y axis on a linear scale, and the y axis on a log scale and the x axis on a linear scale, respectively.

**TRY IT!** Given the lists x = np.arange(11) and $y = x^2$, create a 2 by 3 subplot where each subplot plots x versus y using *plot*, *scatter*, *bar*, *loglog*, *semilogx*, and *semilogy*. Title and label each plot appropriately. Use a grid, but a legend is not necessary. 

x = np.arange(11)
y = x**2

plt.figure(figsize = (14, 8))

plt.subplot(2, 3, 1)
plt.plot(x,y)
plt.title('Plot')
plt.xlabel('X')
plt.ylabel('Y')
plt.grid()

plt.subplot(2, 3, 2)
plt.scatter(x,y)
plt.title('Scatter')
plt.xlabel('X')
plt.ylabel('Y')
plt.grid()

plt.subplot(2, 3, 3)
plt.bar(x,y)
plt.title('Bar')
plt.xlabel('X')
plt.ylabel('Y')
plt.grid()

plt.subplot(2, 3, 4)
plt.loglog(x,y)
plt.title('Loglog')
plt.xlabel('X')
plt.ylabel('Y')
plt.grid(which='both')

plt.subplot(2, 3, 5)
plt.semilogx(x,y)
plt.title('Semilogx')
plt.xlabel('X')
plt.ylabel('Y')
plt.grid(which='both')

plt.subplot(2, 3, 6)
plt.semilogy(x,y)
plt.title('Semilogy')
plt.xlabel('X')
plt.ylabel('Y')
plt.grid()

plt.tight_layout()

plt.show()

We could see that at the end of our plot, we used `plt.tight_layout` to make the sub-figures not overlap with each other, you can try and see the effect without this statement. 

Besides, sometimes, you want to save the figures as a specific format, such as pdf, jpeg, png, and so on. You can do this with the function `plt.savefig`. 

plt.figure(figsize = (8,6))
plt.plot(x,y)
plt.xlabel('X')
plt.ylabel('Y')
plt.savefig('image.pdf')

Finally, there are other functions for plotting data in 2D. The `errorbar` function plots x versus y data but with error bars for each element. The `polar` function plots Î¸ versus r rather than x versus y. The `stem` function plots stems at x with height at y. The `hist` function makes a histogram of a dataset; `boxplot` gives a statistical summary of a dataset; and `pie` makes a pie chart. The usage of these functions are left to your exploration. Do remember to check the examples on the [matplotlib gallery](https://matplotlib.org/gallery/index.html#gallery). 

# 3D Plotting

In order to plot 3D figures use matplotlib, we need to import the *mplot3d* toolkit, which adds the simple 3D plotting capabilities to matplotlib.

import numpy as np
from mpl_toolkits import mplot3d
import matplotlib.pyplot as plt
plt.style.use('seaborn-poster')

Once we imported the *mplot3d* toolkit, we could create 3D axes and add data to the axes. Let's first create a 3D axes. 

fig = plt.figure(figsize = (10,10))
ax = plt.axes(projection='3d')
plt.show()

The *ax = plt.axes(projection='3d')* created a 3D axes object, and to add data to it, we could use *plot3D* function. And we could change the title, set the x,y,z labels for the plot as well. 

**TRY IT!** Consider the parameterized data set t is a vector from 0 to $10\pi$ with a step $\pi/50$, x = sin(t), and y = cos(t). Make a three-dimensional plot of the (x,y,t) data set using *plot3*. Turn the grid on, make the axis equal, and put axis labels and a title. Let's also activate the interactive plot using *%matplotlib notebook*, so that you can move and rotate the figure as well. 

%matplotlib notebook

fig = plt.figure(figsize = (8,8))
ax = plt.axes(projection='3d')
ax.grid()
t = np.arange(0, 10*np.pi, np.pi/50)
x = np.sin(t)
y = np.cos(t)

ax.plot3D(x, y, t)
ax.set_title('3D Parametric Plot')

# Set axes label
ax.set_xlabel('x', labelpad=20)
ax.set_ylabel('y', labelpad=20)
ax.set_zlabel('t', labelpad=20)

plt.show()

Try to rotate the above figure, and get a 3D view of the plot. You may notice that we also set the *labelpad=20* to the 3-axis labels, which will make the label not overlap with the tick texts. 

We could also plot 3D scatter plot using *scatter* function. 

**TRY IT!** Make a 3D scatter plot with randomly generate 50 data points for x, y, and z. Set the point color as red, and size of the point as 50. 

# We can turn off the interactive plot using %matplotlib inline
%matplotlib inline

x = np.random.random(50)
y = np.random.random(50)
z = np.random.random(50)

fig = plt.figure(figsize = (10,10))
ax = plt.axes(projection='3d')
ax.grid()

ax.scatter(x, y, z, c = 'r', s = 50)
ax.set_title('3D Scatter Plot')

# Set axes label
ax.set_xlabel('x', labelpad=20)
ax.set_ylabel('y', labelpad=20)
ax.set_zlabel('z', labelpad=20)

plt.show()

Many times we would like a surface plot rather than a line plot when plotting in three dimensions. In three-dimensional surface plotting, we wish to make a graph of some relationship *f (x, y)*. In surface plotting all (x,y) pairs must be given. This is not straightforward to do using vectors. Therefore, in surface plotting, the first data structure you must create is called a mesh. Given lists/arrays of x and y values, a mesh is a listing of all the possible combinations of x and y. In Python, the mesh is given as two arrays X and Y where X (i,j) and Y (i,j) define possible (x,y) pairs. A third array, Z, can then be created such that Z (i,j) = f (X (i,j), Y (i,j)). A mesh can be created using the *np.meshgrid* function in Python. The *meshgrid* function has the inputs x and y are lists containing the independent data set. The output variables X and Y are as described earlier.

**TRY IT!** Create a mesh for x = [1, 2, 3, 4] and y = [3, 4, 5] using the *meshgrid* function. 

x = [1, 2, 3, 4]
y = [3, 4, 5]

X, Y = np.meshgrid(x, y)
print(X)

print(Y)

We could plot 3D surfaces in Python too, the function to plot the 3D surfaces is *plot_surface(X,Y,Z)*, where X and Y are the output arrays from *meshgrid*, and $Z = f (X,Y)$ or $Z (i,j) = f (X (i,j),Y (i,j))$. The most common surface plotting functions are surf and contour.

**TRY IT!** Make a plot of the surface $f(x,y) = sin(x)\cdot cos(y) for -5\le x\le5, -5\le y\le5$ using the *plot_surface* function. Take care to use a sufficiently fine discretization in x and y to make the plot look smooth. 

fig = plt.figure(figsize = (12,10))
ax = plt.axes(projection='3d')

x = np.arange(-5, 5.1, 0.2)
y = np.arange(-5, 5.1, 0.2)

X, Y = np.meshgrid(x, y)
Z = np.sin(X)*np.cos(Y)

surf = ax.plot_surface(X, Y, Z, cmap = plt.cm.cividis)

# Set axes label
ax.set_xlabel('x', labelpad=20)
ax.set_ylabel('y', labelpad=20)
ax.set_zlabel('z', labelpad=20)

fig.colorbar(surf, shrink=0.5, aspect=8)

plt.show()

You will notice that the surface plot shows different colors for different elevations, yellow for higher and blue for lower, since we used the colormap *plt.cm.cividis* in the surface plot. You can change to different color schemes for the surface plot. These are left as exercises. We also plotted a colorbar to show the corresponding colors to different values. 

We could have subplots of different 3D plots as well. We could use the *add_subplot* function from the figure object we created to generate the subplots for 3D cases. 

'
**TRY IT!** Make a 1 by 2 subplot to plot the above X, Y, Z data in wireframe plot and surface plot. 

fig = plt.figure(figsize=(12,6))


ax = fig.add_subplot(1, 2, 1, projection='3d')
ax.plot_wireframe(X,Y,Z)
ax.set_title('Wireframe plot')

ax = fig.add_subplot(1, 2, 2, projection='3d')
ax.plot_surface(X,Y,Z)
ax.set_title('Surface plot')

plt.tight_layout()

plt.show()

There are many more functions related to plotting in Python and this is in no way an exhaustive list. However, it should be enough to get you started so that you can find the plotting functions in Python that suit you best and provide you with enough background to learn how to use them when you encounter them. You can find more examples of  different type 3D plots on the [mplot3d tutorial website](https://matplotlib.org/mpl_toolkits/mplot3d/tutorial.html). 

<!--NAVIGATION-->
< [12.1 2D Plotting](chapter12.01-2D-Plotting.ipynb) | [Contents](Index.ipynb) | [12.3 Working with Maps](chapter12.03-Working-with-Maps.ipynb) >