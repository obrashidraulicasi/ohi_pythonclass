#!/usr/bin/env python
# coding: utf-8

# # Metodo Newton-Raphson

# Sea $f(x)$ una función suave y continua y $x_r$ una raíz desconocida de $f(x)$. Ahora suponga que $x_0$ es una estimación de $x_r$. A menos que $x_0$ sea una suposición muy afortunada, $f(x_0)$ no será una raíz. Dado este escenario, queremos encontrar un $x_1$ que sea una mejora en $x_0$ (es decir, más cerca de $x_r$ que de $x_0$). Si asumimos que $x_0$ está "lo suficientemente cerca" de $x_r$, podemos mejorarlo tomando la aproximación lineal de $f(x)$ alrededor de $x_0$, que es una línea, y encontrando la intersección de esta línea con el eje x. Escrito, la aproximación lineal de $f(x)$ alrededor de $x_0$ es $f(x) \approx f(x_0) + f^{\prime}(x_0)(x-x_0)$. Usando esta aproximación, encontramos $x_1$ tal que $f(x_1) = 0$. Reemplazar estos valores en la aproximación lineal da como resultado la ecuación
# 
# $$
# 0 = f(x_0) + f^{\prime}(x_0)(x_1-x_0),
# $$
# que cuando se resuelve para $x_1$ es
# $$
# x_1 = x_0 - \frac{f(x_0)}{f^{\prime}(x_0)}.
# $$
# 
# En la siguiente figura se muestra una ilustración de cómo esta aproximación lineal mejora una suposición inicial.
#  
# 
# ![IPython](Newton-step.png )
# 
# Escrito en general, un ** Newton STEP** calcula una suposición mejorada, $x_i$, usando una suposición anterior $x_{i-1}$, y está dada por la ecuación
# 
# $$
# x_i = x_{i-1} - \frac{g(x_{i-1})}{g^{\prime}(x_{i-1})}.
# $$
# 
# El **Método de Newton-Raphson** para encontrar raíces itera los pasos de Newton desde $x_0$ hasta que el error es menor que la tolerancia.
# 
# **TRY IT!** De nuevo, $\sqrt{2}$ es la raíz de la función $f(x) = x^2 - 2$. Usando $x_0 = 1.4$ como punto de partida, usa la ecuación anterior para estimar $\sqrt{2}$. Compare esta aproximación con el valor calculado por la función sqrt de Python.
# 
# $$
# x = 1.4 - \frac{1.4^2 - 2}{2(1.4)} = 1.4142857142857144
# $$

# In[1]:


import numpy as np

f = lambda x: x**2 - 2
f_prime = lambda x: 2*x
newton_raphson = 1.4 - (f(1.4))/(f_prime(1.4))

print("newton_raphson =", newton_raphson)
print("sqrt(2) =", np.sqrt(2))


# **TRY IT!** Escribe una función $my\_newton(f, df, x0, tol)$, donde la salida es una estimación de la raíz de *f*, *f* es un objeto de función $f(x)$, *df* es un objeto de función para $f^{\prime}(x)$, *x0* es una suposición inicial y *tol* es la tolerancia de error. La medida del error debe ser $|f(x)|$.

# In[2]:


def my_newton(f, df, x0, tol):
    # output is an estimation of the root of f 
    # using the Newton Raphson method
    # recursive implementation
    if abs(f(x0)) < tol:
        return x0
    else:
        return my_newton(f, df, x0 - f(x0)/df(x0), tol)


# **TRY IT!** Utilice *my_newton=* para calcular $\sqrt{2}$ con una tolerancia de 1e-6 a partir de *x0 = 1,5*.

# In[3]:


estimate = my_newton(f, f_prime, 1.5, 1e-6)
print("estimate =", estimate)
print("sqrt(2) =", np.sqrt(2))


# Si $x_0$ está cerca de $x_r$, entonces se puede demostrar que, en general, el método de Newton-Raphson converge a $x_r$ mucho más rápido que el método de bisección. Sin embargo, dado que inicialmente se desconoce $x_r$, no hay forma de saber si la suposición inicial está lo suficientemente cerca de la raíz para obtener este comportamiento, a menos que se conozca cierta información especial sobre la función *a priori* (p. ej., la función tiene una raíz cerca de $x = 0$). Además de este problema de inicialización, el método de Newton-Raphson tiene otras limitaciones serias. Por ejemplo, si la derivada estimada es cercana a 0, entonces el paso de Newton será muy grande y probablemente se alejará mucho de la raíz. Además, dependiendo del comportamiento de la función derivada entre $x_0$ y $x_r$, el método de Newton-Raphson puede converger a una raíz diferente a $x_r$ que puede no ser útil para nuestra aplicación de ingeniería.
