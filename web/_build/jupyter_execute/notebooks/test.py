# Reducción de consumo energético

El sistema Captor-4 está compuesto por:

- Sistema de medición: Arduino + shields + sensores
- Gateway/Datalogger: Raspberry Pi + modem 3G
- Bases de datos y VPS en la nube: AWS

```{admonition} Nota
- La **versión base fue denominada 1.0.0** y es a partir de la que se realizaron/sugirieron las modificaciones.
- La  v1.7.0 (de Arduino y RPi) fueron modificaciones **descartadas** para llevar el sistema a bajo consumo según los requerimientos de ese entonces
- Los códigos están disponibles en este documento y también en el grupo https://gitlab.com/captor-nodes. Permisos de owner otorgados a Aina y Zhe.
```


La versión v1.0.0 funciona de modo continuo: mide constantemente, la RPi le solicita cada dos segundos los datos medidos y los almacena en su memoria interna. Cada cierto período pre-configurado estos datos almacenados localmente se suben a la base de datos en AWS mediante  solicitudes http.

El objetivo principal es reducir el consumo energético del nodo Captor-4 de modo que pueda alimentarse con baterías de 5v (power bank) con una autonomía de aproximadamente 6 meses. Esta nueva versión se la denomina **1.7.0**.

Para alcanzar este objetivo se modificará el sistema bajo el siguiente esquema:
- Arduino en modo low power [^lowpowerlib]
- Raspberry Pi apagada con autoencendido cada 2hs mediante el temporizador TPL5110 [^tpl]

[^lowpowerlib]: [Low Power Library](https://github.com/rocketscream/Low-Power)

## Consumo energético

Se midió el consumo energético de los componentes mediante un amperímetro, los valores se detallan en la {numref}`Tabla {number}<tablaconsumos>`.
A modo exploratorio se reprogramaron los arduinos en modo sleep, se le quitaron los LEDs y se modificaron configuraciones de sistema en la Raspberry Pi.  

[^tpl]: [TPL5110](https://www.adafruit.com/product/3435)

```{list-table} Consumo energético por componentes
:header-rows: 1
:name: tablaconsumos

* - Componente
  - Min (mA)
  - Max (mA)
  - Pico (mA)
* - 2 Arduinos + sensores
  - 40
  - 40
  -
* - Raspberry Pi
  - 400
  - 450
  - 
* - Modem 3g
  - 70
  - 190
  - 330
* - Totales
  - 510
  - 680
  - 780
```

Los consumos en los Arduinos se redujeron a un 9% del valor original, no así en la Raspberry Pi por lo que se utilizará un componente electrónico externo de corte energético temporizado denominado TPL5110 [^tpl]. La reducción de consumos se observa en la {numref}`reduccionconsumo`

```{figure} /_static/img/consumo.png
---
name: reduccionconsumo
alt: fishy
width: 800px
align: center
---
Reducción preliminar de consumo promedio
```

## Captor-4 v1.0.0

Una explicación detallada del firmware Arduino del Captor-4 v1.0.0 puede descargarse de [aquí](https://ml-regression.gitlab.io/reporte/_static/img/captor_v100.pdf).    

<iframe src="../_images/captor_v100.pdf" width="70%" height="400px" frameBorder="0"> </iframe>


%```{eval-rst}
%:pdfembed:`src:/_static/m.pdf, height:500, width:500, align:middle`
%```

## Captor-4 v1.7.0

### Memoria disponible

- Memoria de programa (FLASH): 8296 bytes / 30720 bytes (27%)
- Memoria dinámica (SRAM): 1195 bytes / 2048 bytes (58 %)
- Memoria EEPROM: libre (1024 bytes)

### Configuración inicial

En la función `setup()` se configura lo siguiente:

- Referencia analógica 1.1v
- Inicializa timers
- Calcula dirección I2C: base + jumpers: pines 9, 10, 11, 12 (PULLUP)
- Inicializa I2C: dir, eventos: onRequest(), onReceive()

### Funcionamiento general


La refactorización del firmware para llevarlo a bajo consumo consistió en:
- Medir y dormir por el tiempo pre-configurado
- Almacenar la información en memoria SRAM hasta que la Raspberry Pi le solicite datos por I2C.

En forma simplificada el flujo de ejecución se muestra en la {numref}`globalarduino200`.

```{figure} /_static/img/global_arduinov200.png
---
name: globalarduino200
alt: Flujo y eventos
width: 400px
align: center
---
Secuencia de funcionamiento y eventos (firmware 1.7.0)
```

Tomando un período de 15 minutos, el funcionamiento en bajo consumo es el siguiente:

- Estará en *modo sleep* durante un total de 14 minutos, no necesariamente consecutivos.
- Almacenará durante 1 minuto a una frecuencia de 1Hz: 60 `medias móviles`, no necesariamente consecutivas. 
- En la última lectura del minuto conformará el paquete a enviar por I2C, con los promedios excluyendo los valores anómalos.

Esta secuencia no será necesariamente consecutivos, por ejemplo, podría hacerlo en dos veces: leer los sensores durante 30 segundos y dormir por 7 minutos. El tiempo que duerme se denomina `Tsleep` y la cantidad de lecturas continuas `NOBS`. Esto fue parametrizado de modo tal que el único valor a definir es el de `NOBS`. El flujo del programa se observa en la {numref}`mainloopdiag`



```{figure} /_static/img/mainloop_flux_diag.png
---
name: mainloopdiag
alt: Diagrama de la función loop principal  
width: 400px
align: center
---
Diagrama de la función loop principal  (firmware 1.7.0)
```

El código correspondiente al diagrama previo se muestra en la {numref}`ino2loop`.

```{eval-rst}
.. literalinclude:: /_static/src/EQ_V_022.ino
   :language: c
   :caption: Función loop principal - arduino v1.7.0
   :name: ino2loop
   :linenos:
   :lines: 140-158
```

### Lectura de sensores

La lectura de los sensores es idéntica a la versión 1.0.0, consiste en calcular una media móvil de los últimos 5 valores. El procedimiento mostrado en {numref}`sensorreading` puede resumirse en:
- Etapa 1: Cada 10ms se lee uno de las entradas analógicas y se las almacena en un buffer
- Etapa 2: Las nuevas lecturas desplazan a las últimas
- Etapa 3: Al completar el buffer se calcula el promedio (`media móvil`)

```{figure} /_static/img/reading_sensors.png
---
name: sensorreading
alt: Lectura de sensores y media móvil
width: 650px
align: center
---
Lectura de sensores y media móvil (firmware v1.0.0 y 1.7.0)
```

### Almacenamiento de media móvil

Mientras Arduino esté despierto, almacenará cada segundo la media móvil por sensor en un buffer  ({numref}`savemovingaverage`). 

```{figure} /_static/img/save_moving_average.png
---
name: savemovingaverage
alt: save buffer
width: 600px
align: center
---
Almacenamiento de medias móviles (firmware 1.7.0)
```

 Una vez que cada sensor tiene las 60 medias móviles, una por segundo no necesariamente consecutivas, se calcula:
 
- promedio 
- desviación estándar 

Con estos valores calculará ahora un **promedio excluyendo valores anómalos**, esto es, aquellos cuyo `zscore` {eq}`zscore` sea inferior a 2 (p=97.7%, q=1.96).

```{math} zscore = \frac{|x - \mu|}{\sigma}
---
label: zscore
---
```

En el fragmento de la {numref}`Lista {number}<ino2sinoutliers>` se muestra el cálculo de los promedios sin valores anómalos. La función resaltada `build_packet_add_buffer()` se encarga de construir el paquete para I2C con estos valores promediados, y la cantidad de muestras usadas en cada promedio.


```{eval-rst}
.. literalinclude:: /_static/src/EQ_V_022.ino
   :language: c
   :caption: Promedio sin outliers - arduino v1.7.0
   :name: ino2sinoutliers
   :emphasize-lines: 23
   :linenos:
   :lines: 177-199
   :dedent: 8
```

### Construcción del paquete I2C

 Al almacenar los 60 valores de médias móviles se invoca la función `build_packet_add_buffer()` ({numref}`Lista {number}<ino2buildpacket>`), que construye el paquete I2C y lo almacena agrega al buffer de 8 paquetes. El tamaño del paquete I2C es de 25 bytes y su estructura se observa en la {numref}`i2cpacketbuilding`.


```{figure} /_static/img/i2c_packet_v200.png
---
name: i2cpacketbuilding
alt: Construcción del paquete i2c
width: 600px
align: center
---
Construcción del paquete I2C (firmware 1.7.0)
```

En la {numref}`Lista {number}<ino2buildpacket>` se muestra el contenido de `build_packet_add_buffer()` funciona como un buffer circular, una vez que almacenó el octavo paquete comienza por el primero. Se resaltan las siguientes líneas:
- 10: Comienzo de copia de n bytes de un área de memoria a otra
- 17: Cálculo del CRC con todos los bytes previos
- 24: incremento del índice para el próximo paquete


```{eval-rst}
.. literalinclude:: /_static/src/EQ_V_022.ino
   :language: c
   :caption: Contrucción de paquete I2C - arduino v1.7.0
   :name: ino2buildpacket
   :emphasize-lines: 10,17,24
   :linenos:
   :lines: 206,224-248
   
```

### Envío del paquete I2C

El protocolo I2C es del tipo master-slave, donde la Raspberry Pi es quien solicita o envía datos, mientras que el Arduino responde ante estos pedidos. Estos eventos funcionan como interrupciones, despertando al Arduino en caso de recibirlo encontrándose en modo sleep. En el `setup()` se configura la función que debe invocarse ante el evento `onRequest()`. La {numref}`ino2onrequest` muestra este fragmento de código.

```{eval-rst}
.. code-block:: c
    :caption: evento onRequest invoca función send_packet - arduino v1.7.0
    :name: ino2onrequest
    
    Wire.begin(i2c_add);           
    Wire.onRequest(send_packet); 
```

Al recibir una solicitud, Arduino responde envíando el paquete actual y dejando listo el índice para enviar el siguiente en la próxima solicitud. En la {numref}`ino2sendoi2cpacket` se muestra el código correspondiente a la función de envío.

```{eval-rst}
.. literalinclude:: /_static/src/EQ_V_022.ino
   :language: c
   :caption: Respuesta a solicitud I2C - arduino v1.7.0
   :name: ino2sendoi2cpacket
   :linenos:
   :lines: 351-355
```

### Software en la Raspberry Pi

La función `main()` recibe el `PATH` del directorio donde almacenará los datos y un diccionario `DEVICE` cuyas claves son `id` y `addr`, correspondiente al identificador del dispositivo y a su dirección I2C.

La {numref}`Lista {number}<funcionmain>` corresponde a la función `main()` cuyas tareas salientes se encuentran resaltadas:

- 5: Agrega a una lista la respuesta de la lectura de datos del Arduino
- 11: Genera una lista con los paquetes válidos decodificados
- 23: Convierte el unix timestamp en formato `yyyy-mm-dd hh:mm:ss`
- 26: Almacena los datos si hay coincidencia entre el ID recibido en el paquete de datos y el que se esperaba 

```{eval-rst}
.. literalinclude:: /_static/src/tiny_captorv2.py
   :pyobject: main
   :caption: Función main
   :name: funcionmain
   :linenos:
   :emphasize-lines: 5,11,23,26
```

La función `main()` será invocada una vez por cada dispositivo presente, para esto se debe conformar una lista de diccionarios, donde cada diccionario tiene información sobre el identificador y la dirección I2C, tal como se observa en la {numref}`nvecesfuncionmain`

```{eval-rst}
.. literalinclude:: /_static/src/tiny_captorv2.py
   :caption: Llamadas a la función main
   :name: nvecesfuncionmain
   :lines: 150-153
```


Los ocho paquetes correspondientes se observan a continuación.

```
['E1', '2021-03-22 21:18:41', 442, 426, 434, 449, 56, 56, 54, 54, -50.34, 99, 69]
['E1', '2021-03-22 21:30:36', 438, 422, 432, 446, 58, 58, 58, 54, -50.34, 99, 14]
['E1', '2021-03-22 21:46:32', 440, 424, 433, 448, 55, 54, 54, 53, -50.34, 99, 201]
['E1', '2021-03-22 22:02:08', 437, 421, 430, 445, 54, 53, 54, 53, -50.34, 99, 102]
['E1', '2021-03-22 22:17:41', 437, 422, 430, 446, 53, 53, 50, 51, -50.34, 99, 10]
['E1', '2021-03-22 22:35:38', 441, 425, 433, 447, 53, 54, 54, 54, -50.34, 99, 86]
['E1', '2021-03-22 22:50:21', 437, 422, 431, 446, 51, 52, 49, 50, -50.34, 99, 182]
['E1', '2021-03-22 23:03:19', 440, 425, 433, 448, 57, 56, 54, 55, -50.34, 99, 225]
```

Es importante notar que los paquetes se sobreescriben desde el inicio, en la {numref}`rpiantiguos` se muestran resaltados los dos paquetes nuevos y el resto que fueron enviados en la transmisión previa. 

```{eval-rst}
.. code-block:: python
   :caption: : Salida con paquetes antiguos
   :name: rpiantiguos
   :emphasize-lines: 1,2

   ['E1', '2021-03-22 23:20:15', 439, 423, 432, 447, 54, 54, 54, 55, -50.34, 99, 211]
   ['E1', '2021-03-22 23:35:48', 441, 422, 432, 446, 55, 57, 58, 55, -50.34, 99, 131]
   ['E1', '2021-03-22 21:46:32', 440, 424, 433, 448, 55, 54, 54, 53, -50.34, 99, 201]
   ['E1', '2021-03-22 22:02:08', 437, 421, 430, 445, 54, 53, 54, 53, -50.34, 99, 102]
   ['E1', '2021-03-22 22:17:41', 437, 422, 430, 446, 53, 53, 50, 51, -50.34, 99, 10]
   ['E1', '2021-03-22 22:35:38', 441, 425, 433, 447, 53, 54, 54, 54, -50.34, 99, 86]
   ['E1', '2021-03-22 22:50:21', 437, 422, 431, 446, 51, 52, 49, 50, -50.34, 99, 182]
   ['E1', '2021-03-22 23:03:19', 440, 425, 433, 448, 57, 56, 54, 55, -50.34, 99, 225]   
```

Al archivo de salida se le antepone una columna con el timestamp en formato `yyyy-mm-dd hh:mm:ss`, quedando del siguiente modo ({numref}`rpicsvfile`):

```{eval-rst}
.. code-block:: python
   :caption: : Formato de archivo CSV 
   :name: rpicsvfile

    2021-03-24 10:36:17;E1;1616582177;453;429;438;452;55;44;53;53;-50.34;99;202
    2021-03-24 10:51:54;E1;1616583114;453;428;438;452;56;52;56;57;-50.34;99;133
    2021-03-24 09:24:35;E1;1616577875;454;432;438;455;52;46;47;60;-50.34;99;242
    2021-03-24 09:32:18;E1;1616578338;453;431;437;453;38;43;38;60;-50.34;99;164
    2021-03-24 09:47:08;E1;1616579228;452;430;437;453;55;45;33;53;-50.34;99;38
    2021-03-24 10:02:38;E1;1616580158;452;430;437;454;34;54;51;60;-50.34;99;218
    2021-03-24 10:13:49;E1;1616580829;453;429;436;452;52;41;50;52;-50.34;99;116
    2021-03-24 10:24:35;E1;1616581475;453;428;438;452;51;48;52;49;-50.34;99;3
```

```{admonition} Nota
El nombre de archivo se conforma del siguiente modo `{PATH}/data/data{device_id}.csv`, por ejemplo, si el PATH del directorio es `/home/pi/captor/data` y el id del dispositivo `E1`: `/home/pi/captor/data/dataE1.csv`
```


#### Decodificación del paquete I2C

El paquete I2C tiene la siguiente estructura:

```
+--+----+--------+----+--+-+-+---+
|id| ts | AN data| nd |Te|H|C|end|
+--+----+--------+----+--+-+-+---+
```

- 2 `bytes` de ID de dispositiva
- 4 `bytes` de unix timestamp:            
- 8 `bytes` = 4 `int` de promedio de datos
- 4 `bytes` de N datos validos en cada promedio
- 2 `bytes` para temperatura: int (*10 y trunc)
- 1 `byte ` para humedad
- 1 `bytes` de CRC
- 3 `bytes` de fin


En las líneas 9-25 de la {numref}`Lista {number}<funciondecode>` se desempaquetan estos valores crudos, luego entre las líneas 27-33 se convierten a valores numéricos. La temperatura es el único valor flotante que además puede ser negativa, si supera el máximo valor positivo posible de un entero con signo de dos bytes, 32767, significa que hubo un desborde y por tanto es negativa, por lo que se resta a la totalidad de valors posibles el valor de temperatura y multiplica por -1. **Además se lo divide por 100** ya que se lo envió como un  entero para evitar ocupar mas bytes en arduino. Esto es posible por el rango de valores acotado de la temperatura. Se ha resaltado en el código este procedimiento.


```{eval-rst}
.. literalinclude:: /_static/src/tiny_captorv2.py
   :pyobject: decode
   :caption: Función decode()
   :name: funciondecode
   :linenos:
   :emphasize-lines: 34-36
```