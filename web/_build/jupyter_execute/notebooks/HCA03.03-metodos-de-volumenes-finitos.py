#!/usr/bin/env python
# coding: utf-8

# # Método de Volúmenes Finitos

# Apunte teorico [AQUI](https://gitlab.com/ldominguezruben/hidrodinamica-de-cuerpos-de-agua-fich-unl/-/blob/master/web/book/notebooks/ref/M_todo_de_Vol_menes_Finitos.pdf)

# Otro apunte teorico [AQUI](https://gitlab.com/ldominguezruben/hidrodinamica-de-cuerpos-de-agua-fich-unl/-/blob/master/web/book/notebooks/ref/M_todo_de_Vol_menes_Finitos.pdf)

# ![IPython](images/volumenes-finitos.png)

# El Método de Volumen Finito (FVM) es el nombre que se le da a la técnica por la cual la formulación integral de las leyes de conservación se discretiza directamente en el ámbito físico-
# espacio.
# Es el método más aplicado hoy en día en CFD, y es probable que siga siéndolo en el futuro previsible. El motivo de esto radica en su generalidad, su simplicidad conceptual y su facilidad de implementación para cuadrículas arbitrarias, estructuradas así como no estructurado.
# Por lo tanto, incluso para un primer curso en CFD, Conocer los conceptos básicos de la FVM, ya que también tiene muchas implicaciones. para la comprensión de la naturaleza y propiedades de los resultados obtenidos de una simulación CFD. Como veremos a continuación, la FVM se basa en promedios de celda, que aparecen como una cantidad fundamental en CFD. Esto distingue al FVM de los métodos de diferencia finita y elemento finito, donde el valor numérico principal las cantidades son los valores de la función local en los puntos de malla.
# Una vez generada una grilla, la FVM consiste en asociar un volumen finito local, también llamado volumen de control, a cada punto de malla y aplicando la integral ley de conservación a este volumen local. Ésta es una primera distinción importante del metodo de diferencias finitas, donde el espacio discretizado se considera como un conjunto de puntos, mientras que en la FVM el espacio discretizado está formado por un conjunto de celdas pequeñas, siendo una celda asociado a un punto de malla.
# Una ventaja esencial de la FVM está relacionada con el concepto muy importante de discretización conservadora. Es de gran importancia mantener la globalización conservación de las cantidades básicas de flujo, masa, momento y energía, en el discreto nivel y esto pone condiciones en la forma en que el proceso de discretización de las ecuaciones es realizado. La FVM tiene la gran ventaja de que la discretización conservadora se satisface automáticamente, a través de la discretización directa de la forma integral del leyes de conservación. El método de volumen finito saca todas sus ventajas en una malla arbitraria, donde hay un gran número de opciones abiertas para la definición de los volúmenes de control en los que se expresan las leyes de conservación. Modificar la forma y ubicación del control volúmenes asociados a un punto de malla dado, además de variar las reglas y la precisión para la evaluación de los flujos a través de las superficies de control, da una considerable flexibilidad al método de volumen finito. Esto explica la generalidad de la FVM y
# por supuesto, deben cumplirse una serie de reglas durante estas operaciones.

# ![IPython](images/mesh.png)

# 
