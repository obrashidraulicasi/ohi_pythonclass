# Calculamos con Python

La primera etapa es conocer los alcances del uso de Python y en especial los que involucra al curso. A continuación introduciremos algunas demostraciones respecto a operaciones aritmeticas como adición, resta, multiplicación, división y potencia. Un operador **arithmetic operator** es un simbolo que Python reserva para las operaciones indicadas anteriormente. Los simbolos en Python usados son + para la suma, - para la resta, * para multiplicación, / para division, y \** para potencia.

Hagamos un par de ejemplos de dichas operaciones en Jupyter-Lab. Ejecutemos en en Anaconda Navigator Jupyter-Lab.

**Intentemos sumar, restar y multiplicar. 

5+6

10-9

15*2

Un **orden de operaciones** es un orden de precedencia estándar que tienen diferentes operaciones en relación entre sí. Python utiliza el mismo orden de operaciones que aprendiste en la escuela primaria. Los poderes se ejecutan antes de la multiplicación y la división, que se ejecutan antes de la suma y la resta. Los paréntesis, (), también se pueden usar en Python para reemplazar el orden estándar de operaciones.


**Intentamos computar** $\frac{3*4}{(2^2+8/2)}$.

(3*4)/(2**2 + 8/2)

**TIP!** Se puede usar el símbolo _ para representar este resultado para dividir expresiones complicadas en comandos más simples.

Python puede manipular las funciones aritmeticas como `sin`, `cos`, `tan`, `asin`, `acos`, `atan`, `exp`, `log`, `log10` y `sqrt`, las cuales pueden ser ejecutadas con la libreria **math**. Importemos la libreria para poder acceder a las funciones.

import math

**TIP!** En el Jupyter notebook y en Ipython, puede tener una vista rápida de lo que hay en el módulo escribiendo el `nombre del módulo + punto + TAB`. Además, si escribe las primeras letras de la función y presiona TAB, completa automáticamente la función.

La forma en que usamos estas funciones matemáticas es `modulo.function`, los argumentos se colocan entre paréntesis. Para funciones trigonométricas, es útil tener disponible el valor de $\ pi$. Se puede para ello llamar en cualquier momento escribiendo `math.pi` en la celda de código.

math.sqrt(9)

**Intentemos computar**  $cos(\frac{\pi}{5})$.

math.cos(math.pi/5)

Python compondrá funciones como es de esperar, con la función más interna ejecutándose primero. Lo mismo ocurre con las llamadas a funciones que se componen con operaciones aritméticas.

Tenga en cuenta que la función `log` en Python es $ log_e $, o el logaritmo natural. No es $ log_ {10} $. Si desea usar $ log_ {10} $, debe usar `math.log10`.

math.exp(math.log(10))

Muchas veces, cuando usamos una función en Python, queremos saber que hace. En el cuaderno Ipython o Jupyter, podemos solicitar esa información (de manera resumida) escribiendo `function?`, Es decir, el signo de interrogación es un atajo para obtener ayuda.

math.factorial?

Cuantdo tengamos una expresion dividida por cero, cuyo resultado es infinito. Python generará un `ZeroDivisionError`.

1/0

Puede escribir `math.inf` en el símbolo del sistema para indicar infinito o` math.nan` para indicar algo que no es un número que desea que se maneje como un número. Si esto es confuso, esta distinción se puede omitir por ahora; se explicará con más claridad cuando sea importante.

 10/math.inf

math.inf * 8

math.inf/math.inf

 Finalmente, Python también puede manejar el número imaginario.

2+ 8j

complex(2,8)

Python también permite manejar la notación científica usando la letra e entre dos números. Por ejemplo, $ 1e6 = 1000000 $ y $ 1e-3 = 0.001 $.

3e0*3.65e2*2.4e1*3.6e3

** TIP! ** Cada vez que escribimos la función en el módulo * math *, siempre escribimos `math.function_name`. Alternativamente, hay una forma más sencilla, por ejemplo, si queremos usar `sin` y` log` del módulo `math`, podríamos importarlos de esta manera:` from math import sin, log`. Entonces todo lo que necesita hacer al usar estas funciones es usarlas directamente, por ejemplo, `sin (10)` o `log (10)`.

from math import sin

sin(10)

## Tipos de datos

Acabamos de aprender a usar Python como una calculadora para manejar diferentes valores de datos. En Python, hay algunos tipos de datos que necesitamos conocer, ya que los valores numéricos, int, float y complex son los tipos asociados con los valores.

* **int**: Enteros, tales como 1, 2, 3, ...
* **float**: Flotantes (decimales) 3.2, 6.4, ...
* **complex**: Numeros complejos, tales como 2 + 5j, 3 + 2j, ...
* **str**: Letras, 'a', 'b', 'Hola',...

Se puede usar la función **type** para chequear que tipo de dato es el valor que usamos.


type(1234)

type(3.14)

type(2 + 5j)

type('Hola')