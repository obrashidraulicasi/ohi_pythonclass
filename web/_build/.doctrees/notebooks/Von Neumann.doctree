���2      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�Von Neumann Analysis�h]�h	�Text����Von Neumann Analysis�����}�(hh�parent�huba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�h�line�M'�source��lF:\projects\facultad\posgrado\hidrodinamica-de-cuerpos-de-agua-fich-unl\web\book\notebooks\Von Neumann.ipynb�hhubh	�	paragraph���)��}�(h�OIn this notebook, we will explore the spectral properties of advection schemes.�h]�h�OIn this notebook, we will explore the spectral properties of advection schemes.�����}�(hh0hh.hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h,h)M'h*h+hhhhubh-)��}�(h��To run each of the following cells, use the keyboard shortcut **SHIFT** + **ENTER**, press the button ``Run`` in the toolbar or find the option ``Cell > Run Cells`` from the menu bar. For more shortcuts, see ``Help > Keyboard Shortcuts``.�h]�(h�>To run each of the following cells, use the keyboard shortcut �����}�(h�>To run each of the following cells, use the keyboard shortcut �hh<hhh*Nh)Nubh	�strong���)��}�(h�SHIFT�h]�h�SHIFT�����}�(h�SHIFT�hhGhhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hEh)M'h*h+hh<hhubh� + �����}�(h� + �hh<hhh*Nh)NubhF)��}�(h�ENTER�h]�h�ENTER�����}�(h�ENTER�hh[hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hEh)M'h*h+hh<hhubh�, press the button �����}�(h�, press the button �hh<hhh*Nh)Nubh	�literal���)��}�(h�Run�h]�h�Run�����}�(hhhhqhhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hoh)M'h*h+hh<hhubh�# in the toolbar or find the option �����}�(h�# in the toolbar or find the option �hh<hhh*Nh)Nubhp)��}�(h�Cell > Run Cells�h]�h�Cell > Run Cells�����}�(hhhh�hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hoh)M'h*h+hh<hhubh�, from the menu bar. For more shortcuts, see �����}�(h�, from the menu bar. For more shortcuts, see �hh<hhh*Nh)Nubhp)��}�(h�Help > Keyboard Shortcuts�h]�h�Help > Keyboard Shortcuts�����}�(hhhh�hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hoh)M'h*h+hh<hhubh�.�����}�(h�.�hh<hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)M'h*h+hhhhubh-)��}�(h�MTo get started, import the required Python modules by running the cell below.�h]�h�MTo get started, import the required Python modules by running the cell below.�����}�(hh�hh�hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h,h)M'h*h+hhhhub�myst_nb.nodes��CellNode���)��}�(hhh]�h��CellInputNode���)��}�(hhh]�h	�literal_block���)��}�(h��# Configuration for visualizing the plots
%matplotlib notebook
%config InlineBackend.figure_format = 'retina'

# Required modules
import numpy as np
import matplotlib.pyplot as plt

# Import figure style and custom functions
import nbtools as nb�h]�h��# Configuration for visualizing the plots
%matplotlib notebook
%config InlineBackend.figure_format = 'retina'

# Required modules
import numpy as np
import matplotlib.pyplot as plt

# Import figure style and custom functions
import nbtools as nb�����}�(hhhh�ubah}�(h]�h ]�h"]�h$]�h&]��	xml:space��preserve��language��ipython3�uh(h�hh�hhh*h+h)K ubah}�(h]�h ]��
cell_input�ah"]�h$]�h&]�uh(h�h)M"Nh*h+hh�hhubah}�(h]�h ]��cell�ah"]�h$]�h&]��	cell_type��code�uh(h�hhhhh*h+h)K ubh-)��}�(h�TWe define the amplification factor for the **explicit first-order advection** scheme�h]�(h�+We define the amplification factor for the �����}�(h�+We define the amplification factor for the �hh�hhh*Nh)NubhF)��}�(h�explicit first-order advection�h]�h�explicit first-order advection�����}�(h�explicit first-order advection�hh�hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hEh)M2uh*h+hh�hhubh� scheme�����}�(h� scheme�hh�hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)M2uh*h+hhhhubh�)��}�(hhh]�h�)��}�(hhh]�h�)��}�(h�idef ampf_advection_explicit(sigma, kmdx):
    amp = np.abs(1-sigma+sigma*np.exp(-1j*kmdx))
    return amp�h]�h�idef ampf_advection_explicit(sigma, kmdx):
    amp = np.abs(1-sigma+sigma*np.exp(-1j*kmdx))
    return amp�����}�(hhhj  ubah}�(h]�h ]�h"]�h$]�h&]�h�h�h�h�uh(h�hj  hhh*h+h)K ubah}�(h]�h ]�h�ah"]�h$]�h&]�uh(h�h)MB�h*h+hj  hhubah}�(h]�h ]�h�ah"]�h$]�h&]��	cell_type��code�uh(h�hhhhh*h+h)K ubh-)��}�(h�VThen, we define the CFL numbers ``sigmas`` and wavenumbers times grid spacing ``kmdx``�h]�(h� Then, we define the CFL numbers �����}�(h� Then, we define the CFL numbers �hj2  hhh*Nh)Nubhp)��}�(h�sigmas�h]�h�sigmas�����}�(hhhj;  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hoh)MR�h*h+hj2  hhubh�$ and wavenumbers times grid spacing �����}�(h�$ and wavenumbers times grid spacing �hj2  hhh*Nh)Nubhp)��}�(h�kmdx�h]�h�kmdx�����}�(hhhjN  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hoh)MR�h*h+hj2  hhubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)MR�h*h+hhhhubh�)��}�(hhh]�h�)��}�(hhh]�h�)��}�(h�tsigma_start = 0.5
sigma_end = 1.0

sigmas = np.linspace(sigma_start, sigma_end, 6)
kmdx = np.linspace(0, np.pi, 100)�h]�h�tsigma_start = 0.5
sigma_end = 1.0

sigmas = np.linspace(sigma_start, sigma_end, 6)
kmdx = np.linspace(0, np.pi, 100)�����}�(hhhjh  ubah}�(h]�h ]�h"]�h$]�h&]�h�h�h�h�uh(h�hje  hhh*h+h)K ubah}�(h]�h ]�h�ah"]�h$]�h&]�uh(h�h)Mb�h*h+hjb  hhubah}�(h]�h ]�h�ah"]�h$]�h&]��	cell_type��code�uh(h�hhhhh*h+h)K ubh-)��}�(h�0Generate the plots by running the following cell�h]�h�0Generate the plots by running the following cell�����}�(hj�  hj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h,h)Jr h*h+hhhhubh�)��}�(hhh]�h�)��}�(hhh]�h�)��}�(hXS  # Initialize plot and colors
fig, ax = plt.subplots()
colors = nb.get_colors(len(sigmas))

for sigma, color in zip(sigmas, colors):
    amp = ampf_advection_explicit(sigma, kmdx)
    ax.plot(kmdx, amp, color=color, label=f'$\sigma={sigma:.1f}$')

# Display legend
ax.legend()

# Set x and y labels
ax.set_xlabel(r'$\kappa_m \Delta x$')
ax.set_ylabel(r'$|e^{a\Delta t}|$')

# Manually set tick labels as multiples of pi/4 (must change if range changes)
plt.xticks([0, np.pi/4, np.pi/2, 3*np.pi/4, np.pi], 
           ['0', r'$\frac{\pi}{4}$',  r'$\frac{\pi}{2}$',  r'$\frac{3\pi}{4}$', r'$\pi$'])�h]�hXS  # Initialize plot and colors
fig, ax = plt.subplots()
colors = nb.get_colors(len(sigmas))

for sigma, color in zip(sigmas, colors):
    amp = ampf_advection_explicit(sigma, kmdx)
    ax.plot(kmdx, amp, color=color, label=f'$\sigma={sigma:.1f}$')

# Display legend
ax.legend()

# Set x and y labels
ax.set_xlabel(r'$\kappa_m \Delta x$')
ax.set_ylabel(r'$|e^{a\Delta t}|$')

# Manually set tick labels as multiples of pi/4 (must change if range changes)
plt.xticks([0, np.pi/4, np.pi/2, 3*np.pi/4, np.pi], 
           ['0', r'$\frac{\pi}{4}$',  r'$\frac{\pi}{2}$',  r'$\frac{3\pi}{4}$', r'$\pi$'])�����}�(hhhj�  ubah}�(h]�h ]�h"]�h$]�h&]�h�h�h�h�uh(h�hj�  hhh*h+h)K ubah}�(h]�h ]�h�ah"]�h$]�h&]�uh(h�h)J�8 h*h+hj�  hhubah}�(h]�h ]�h�ah"]�h$]�h&]��	cell_type��code�uh(h�hhhhh*h+h)K ubh-)��}�(h�BNow try the amplification factor for the implicit advection scheme�h]�h�BNow try the amplification factor for the implicit advection scheme�����}�(hj�  hj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h,h)J�_ h*h+hhhhubh�)��}�(hhh]�h�)��}�(hhh]�h�)��}�(h��def ampf_advection_implicit(sigma, kmdx):
    # Complete the following line with the implicit advection amplification factor
    amp = 
    return amp�h]�h��def ampf_advection_implicit(sigma, kmdx):
    # Complete the following line with the implicit advection amplification factor
    amp = 
    return amp�����}�(hhhj�  ubah}�(h]�h ]�h"]�h$]�h&]�h�h�h�h�uh(h�hj�  hhh*h+h)K ubah}�(h]�h ]�h�ah"]�h$]�h&]�uh(h�h)J�� h*h+hj�  hhubah}�(h]�h ]�h�ah"]�h$]�h&]��	cell_type��code�uh(h�hhhhh*h+h)K ubh-)��}�(h��Since this scheme is unconditionally stable, we show the results for larger values of ``sigma``. Generate curves for six equispaced values of $\sigma\in[0,10]$, following the code in the second cell.�h]�(h�VSince this scheme is unconditionally stable, we show the results for larger values of �����}�(h�VSince this scheme is unconditionally stable, we show the results for larger values of �hj�  hhh*Nh)Nubhp)��}�(h�sigma�h]�h�sigma�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hoh)J�� h*h+hj�  hhubh�/. Generate curves for six equispaced values of �����}�(h�/. Generate curves for six equispaced values of �hj�  hhh*Nh)Nubh	�math���)��}�(h�\sigma\in[0,10]�h]�h�\sigma\in[0,10]�����}�(hhhj  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(j   h)J�� h*h+hj�  hhubh�(, following the code in the second cell.�����}�(h�(, following the code in the second cell.�hj�  hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)J�� h*h+hhhhubh�)��}�(hhh]�h�)��}�(hhh]�h�)��}�(h�E# Generate new values of sigma here
sigma_start =
sigma_end = 

# ...�h]�h�E# Generate new values of sigma here
sigma_start =
sigma_end = 

# ...�����}�(hhhj!  ubah}�(h]�h ]�h"]�h$]�h&]�h�h�h�h�uh(h�hj  hhh*h+h)K ubah}�(h]�h ]�h�ah"]�h$]�h&]�uh(h�h)J�� h*h+hj  hhubah}�(h]�h ]�h�ah"]�h$]�h&]��	cell_type��code�uh(h�hhhhh*h+h)K ubh-)��}�(h�CComplete line ``6`` in the following cell and generate the new plot�h]�(h�Complete line �����}�(h�Complete line �hj=  hhh*Nh)Nubhp)��}�(h�6�h]�h�6�����}�(hhhjF  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hoh)J�� h*h+hj=  hhubh�0 in the following cell and generate the new plot�����}�(h�0 in the following cell and generate the new plot�hj=  hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)J�� h*h+hhhhubh�)��}�(hhh]�h�)��}�(hhh]�h�)��}�(hXC  # Initialize plot and colors
fig, ax = plt.subplots()
colors = nb.get_colors(len(sigmas))

for sigma, color in zip(sigmas, colors):
    amp = # Complete code here
    ax.plot(kmdx, amp, color=color, label=f'$\sigma={sigma:.0f}$')

# Display legend
ax.legend()

# Set x and y labels
ax.set_xlabel(r'$\kappa_m \Delta x$')
ax.set_ylabel(r'$|e^{a\Delta t}|$')

# Manually set tick labels as multiples of pi/4 (must change if range changes)
plt.xticks([0, np.pi/4, np.pi/2, 3*np.pi/4, np.pi], 
           ['0', r'$\frac{\pi}{4}$',  r'$\frac{\pi}{2}$',  r'$\frac{3\pi}{4}$', r'$\pi$'])�h]�hXC  # Initialize plot and colors
fig, ax = plt.subplots()
colors = nb.get_colors(len(sigmas))

for sigma, color in zip(sigmas, colors):
    amp = # Complete code here
    ax.plot(kmdx, amp, color=color, label=f'$\sigma={sigma:.0f}$')

# Display legend
ax.legend()

# Set x and y labels
ax.set_xlabel(r'$\kappa_m \Delta x$')
ax.set_ylabel(r'$|e^{a\Delta t}|$')

# Manually set tick labels as multiples of pi/4 (must change if range changes)
plt.xticks([0, np.pi/4, np.pi/2, 3*np.pi/4, np.pi], 
           ['0', r'$\frac{\pi}{4}$',  r'$\frac{\pi}{2}$',  r'$\frac{3\pi}{4}$', r'$\pi$'])�����}�(hhhje  ubah}�(h]�h ]�h"]�h$]�h&]�h�h�h�h�uh(h�hjb  hhh*h+h)K ubah}�(h]�h ]�h�ah"]�h$]�h&]�uh(h�h)J�" h*h+hj_  hhubah}�(h]�h ]�h�ah"]�h$]�h&]��	cell_type��code�uh(h�hhhhh*h+h)K ubeh}�(h]��von-neumann-analysis�ah ]�h"]��von neumann analysis�ah$]�h&]�uh(h
h)M'h*h+hhhhubah}�(h]�h ]�h"]�h$]�h&]��source�h+uh(h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h+�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�j�  j�  s�	nametypes�}�j�  Nsh}�j�  hs�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]��transform_messages�]��transformer�N�
decoration�Nhh�fm_substitutions�}�ub.