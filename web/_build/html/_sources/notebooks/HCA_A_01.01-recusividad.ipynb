{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "source": [
    "# Recursividad"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "source": [
    "A **recursive** function is a function that makes calls to itself. It works like the loops we described before, but sometimes it the situation is better to use recursion than loops. \n",
    "\n",
    "Every recursive function has two components: a __base case__ and a __recursive step__. The __base case__ is usually the smallest input and has an easily verifiable solution. This is also the mechanism that stops the function from calling itself forever. The __recursive step__ is the set of all cases where a __recursive call__, or a function call to itself, is made."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "source": [
    "As an example, we show how recursion can be used to define and compute the factorial of an integer number. The factorial of an integer $n$ is $1 \\times 2 \\times 3 \\times ... \\times (n - 1) \\times n$. The recursive definition can be written:\n",
    "\n",
    "\\begin{equation}\n",
    "f(n) = \\begin{cases}\n",
    "    1 &\\text{if $n=1$}\\\\\n",
    "    n \\times f(n-1) & \\text{otherwise}\\\\\n",
    "    \\end{cases}\n",
    "\\end{equation}\n",
    "\n",
    "The base case is $n = 1$ which is trivial to compute: $f(1) = 1$. In the recursive step, $n$ is multiplied by the result of a recursive call to the factorial of $n - 1$.\n",
    "\n",
    "**TRY IT!** Write the factorial function using recursion. Use your function to compute the factorial of 3. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "def factorial(n):\n",
    "    \"\"\"Computes and returns the factorial of n, \n",
    "    a positive integer.\n",
    "    \"\"\"\n",
    "    if n == 1: # Base cases!\n",
    "        return 1\n",
    "    else: # Recursive step\n",
    "        return n * factorial(n - 1) # Recursive call     "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "6"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "factorial(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "source": [
    "__WHAT IS HAPPENING?__ First recall that when Python executes a function, it creates a workspace for the variables that are created in that function, and whenever a function calls another function, it will wait until that function returns an answer before continuing. In programming, this workspace is called stack. Similar to a stack of plates in our kitchen, elements in a stack are added or removed from the top of the stack to the bottom, in a \"last in, first out\" order. For example, in the `np.sin(np.tan(x))`, `sin` must wait for `tan` to return an answer before it can be evaluated. Even though a recursive function makes calls to itself, the same rules apply.\n",
    "\n",
    "1. A call is made to `factorial(3)`, A new workspace is opened to compute `factorial(3)`.\n",
    "2. Input argument value 3 is compared to 1. Since they are not equal, else statement is executed.\n",
    "3. `3*factorial(2)` must be computed. A new workspace is opened to compute `factorial(2)`.\n",
    "4. Input argument value 2 is compared to 1. Since they are not equal, else statement is executed.\n",
    "5. `2*factorial(1)` must be computed. A new workspace is opened to compute `factorial(1)`.\n",
    "6. Input argument value 1 is compared to 1. Since they are equal, if statement is executed.\n",
    "7. The return variable is assigned the value 1. `factorial(1)` terminates with output 1. \n",
    "8. `2*factorial(1)` can be resolved to $2 \\times 1 = 2$. Output is assigned the value 2. `factorial(2)` terminates with output 2. \n",
    "9. `3*factorial(2)` can be resolved to $3 \\times 2 = 6$. Output is assigned the value 6. `factorial(3)` terminates with output 6. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "source": [
    "The order of recursive calls can be depicted by a recursion tree shown in the following figure for `factorial(3)`. A recursion tree is a diagram of the function calls connected by numbered arrows to depict the order in which the calls were made. \n",
    "\n",
    "<img src=\"images/06.01.01-Recursion_tree_for_factorial.png\" title=\"Recursion tree for factorial(3)\" width=\"200\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "source": [
    "Fibonacci numbers were originally developed to model the idealized population growth of rabbits. Since then, they have been found to be significant in any naturally occurring phenomena. The Fibonacci numbers can be generated using the following recursive formula. Note that the recursive step contains two recursive calls and that there are also two base cases (i.e., two cases that cause the recursion to stop).\n",
    "\n",
    "\\begin{equation}\n",
    "F(n) = \\begin{cases}\n",
    "    1 &\\text{if $n=1$}\\\\\n",
    "    1 &\\text{if $n=2$}\\\\\n",
    "    F(n-1) + F(n-2) & \\text{otherwise}\\\\\n",
    "    \\end{cases}\n",
    "\\end{equation}\n",
    "\n",
    "**TRY IT!** Write a recursive function for computing the *n-th* Fibonacci number. Use your function to compute the first five Fibonacci numbers. Draw the associated recursion tree. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "def fibonacci(n):\n",
    "    \"\"\"Computes and returns the Fibonacci of n, \n",
    "    a postive integer.\n",
    "    \"\"\"\n",
    "    if n == 1: # first base case\n",
    "        return 1\n",
    "    elif n == 2: # second base case\n",
    "        return 1\n",
    "    else: # Recursive step\n",
    "        return fibonacci(n-1) + fibonacci(n-2) # Recursive call "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1\n",
      "1\n",
      "2\n",
      "3\n",
      "5\n"
     ]
    }
   ],
   "source": [
    "print(fibonacci(1))\n",
    "print(fibonacci(2))\n",
    "print(fibonacci(3))\n",
    "print(fibonacci(4))\n",
    "print(fibonacci(5))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "source": [
    "![fibonacci(5)](images/06.01.02-Recursion_tree_for_fibonacci.png \"Recursion tree for fibonacci(5)\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "source": [
    "As an exercise, consider the following modification to `fibonacci`, where the results of each recursive call are displayed to the screen.\n",
    "\n",
    "**EXAMPLE:** Write a function `fibonacci_display` that based on the Modification of `fibonacci`. Can you determine the order in which the Fibonacci numbers will appear on the screen for fibonacci(5)?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "def fibonacci_display(n):\n",
    "    \"\"\"Computes and returns the Fibonacci of n, \n",
    "    a postive integer.\n",
    "    \"\"\"\n",
    "    if n == 1: # first base case\n",
    "        out = 1\n",
    "        print(out)\n",
    "        return out\n",
    "    elif n == 2: # second base case\n",
    "        out = 1\n",
    "        print(out)\n",
    "        return out\n",
    "    else: # Recursive step\n",
    "        out = fibonacci_display(n-1)+fibonacci_display(n-2)\n",
    "        print(out)\n",
    "        return out # Recursive call "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1\n",
      "1\n",
      "2\n",
      "1\n",
      "3\n",
      "1\n",
      "1\n",
      "2\n",
      "5\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "5"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "fibonacci_display(5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "source": [
    "Notice that the number of recursive calls becomes very large for even relatively small inputs for `n`. If you do not agree, try to draw the recursion tree for `fibonacci(10)`. If you try your unmodified function for inputs around 35, you will notice significant computation times."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "9227465"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "fibonacci(35)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "source": [
    "There is an iterative method of computing the *n-th* Fibonacci number that requires only one workspace.\n",
    "\n",
    "**EXAMPLE:** Iterative implementation for computing Fibonacci numbers."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "def iter_fib(n):\n",
    "    fib = np.ones(n)\n",
    "    \n",
    "    for i in range(2, n):\n",
    "        fib[i] = fib[i - 1] + fib[i - 2]\n",
    "        \n",
    "    return fib"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "source": [
    "**TRY IT!** Compute the *25-th* Fibonacci number using `iter_fib` and `fibonacci`. And use the magic command `timeit` to measure the run time for each. Notice the large difference in running times. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "8.52 Âµs Â± 141 ns per loop (mean Â± std. dev. of 7 runs, 100000 loops each)\n"
     ]
    }
   ],
   "source": [
    "%timeit iter_fib(25)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "16.5 ms Â± 260 Âµs per loop (mean Â± std. dev. of 7 runs, 100 loops each)\n"
     ]
    }
   ],
   "source": [
    "%timeit fibonacci(25)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "source": [
    "You can see in the previous example that the iterative version runs much faster than the recursive counterpart. In general, iterative functions are faster than recursive functions that perform the same task. So why use recursive functions at all? There are some solution methods that have a naturally recursive structure. In these cases it is usually very hard to write a counterpart using loops. The primary value of writing recursive functions is that they can usually be written much more compactly than iterative functions. The cost of the improved compactness is added running time.\n",
    "\n",
    "The relationship between the input arguments and the running time is discussed in more detail later in the chapter on Complexity.\n",
    "\n",
    "**TIP!** Try to write functions iteratively whenever it is convenient to do so. Your functions will run faster. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**NOTE!** When we are using recursive call as showing above, we need to make sure that it can reach the base case, otherwise, it results to infinite recursion. In Python, when we execute a recursive function on a large output that can not reach the base case, we will encounter a \"maximum recursion depth exceeded error\". Try the following example, and see what do you get. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "ename": "RecursionError",
     "evalue": "maximum recursion depth exceeded in comparison",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mRecursionError\u001b[0m                            Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-11-4d2572cc43ba>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m\u001b[0m\n\u001b[0;32m----> 1\u001b[0;31m \u001b[0mfactorial\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;36m5000\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[0;32m<ipython-input-1-d6624133408f>\u001b[0m in \u001b[0;36mfactorial\u001b[0;34m(n)\u001b[0m\n\u001b[1;32m      6\u001b[0m         \u001b[0;32mreturn\u001b[0m \u001b[0;36m1\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m      7\u001b[0m     \u001b[0;32melse\u001b[0m\u001b[0;34m:\u001b[0m \u001b[0;31m# Recursive step\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m----> 8\u001b[0;31m         \u001b[0;32mreturn\u001b[0m \u001b[0mn\u001b[0m \u001b[0;34m*\u001b[0m \u001b[0mfactorial\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mn\u001b[0m \u001b[0;34m-\u001b[0m \u001b[0;36m1\u001b[0m\u001b[0;34m)\u001b[0m \u001b[0;31m# Recursive call\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m",
      "... last 1 frames repeated, from the frame below ...\n",
      "\u001b[0;32m<ipython-input-1-d6624133408f>\u001b[0m in \u001b[0;36mfactorial\u001b[0;34m(n)\u001b[0m\n\u001b[1;32m      6\u001b[0m         \u001b[0;32mreturn\u001b[0m \u001b[0;36m1\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m      7\u001b[0m     \u001b[0;32melse\u001b[0m\u001b[0;34m:\u001b[0m \u001b[0;31m# Recursive step\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m----> 8\u001b[0;31m         \u001b[0;32mreturn\u001b[0m \u001b[0mn\u001b[0m \u001b[0;34m*\u001b[0m \u001b[0mfactorial\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mn\u001b[0m \u001b[0;34m-\u001b[0m \u001b[0;36m1\u001b[0m\u001b[0;34m)\u001b[0m \u001b[0;31m# Recursive call\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[0;31mRecursionError\u001b[0m: maximum recursion depth exceeded in comparison"
     ]
    }
   ],
   "source": [
    "factorial(5000)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can handle the recursion limit using the `sys` module in Python and set a higher limit. Run the following code, and you will not see the error. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sys\n",
    "sys.setrecursionlimit(10**5)\n",
    "\n",
    "factorial(5000)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "source": [
    "## Divide and Conquer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "source": [
    "**Divide and conquer** is a useful strategy for solving difficult problems. Using divide and conquer, difficult problems are solved from solutions to many similar easy problems. In this way, difficult problems are broken up so they are more manageable. In this section, we cover two classical examples of divide and conquer: the Towers of Hanoi Problem and the Quicksort algorithm."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "source": [
    "## Towers of Hanoi\n",
    "\n",
    "The Towers of Hanoi problem consists of three vertical rods, or towers, and *N* disks of different sizes, each with a hole in the center so that the rod can slide through it. The disks are originally stacked on one of the towers in order of descending size (i.e., the largest disc is on the bottom). The goal of the problem is to move all the disks to a different rod while complying with the following three rules:\n",
    "\n",
    "\n",
    "1. Only one disk can be moved at a time.\n",
    "2. Only the disk at the top of a stack may be moved. \n",
    "3. A disk may not be placed on top of a smaller disk.\n",
    "\n",
    "The following figure shows the steps of the solution to the Tower of Hanoi problem with three disks. \n",
    "\n",
    "![Tower](images/06.02.01-Illustration_Towers_of_Hanoi.png \"Illustration of the Towers of Hanoi: In eight steps, all disks are transported from pole 1 to pole 3, one at a time, by moving only the disk at the top of the current stack, and placing only smaller disks on top of larger disks.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "source": [
    "A legend goes that a group of Indian monks are in a monastery working to complete a Towers of Hanoi problem with 64 disks. When they complete the problem, the world will end. Fortunately, the number of moves required is $2^{64} âˆ’ 1$ so even if they could move one disk per millisecond, it would take over 584 million years for them to finish.\n",
    "\n",
    "The key to the Towers of Hanoi problem is breaking it down into smaller, easier-to-manage problems that we will refer to as **subproblems**. For this problem, it is relatively easy to see that moving a disk is easy (which has only three rules) but moving a tower is difficult (we cannot immediately see how to do it). So we will assign moving a stack of size *N* to several subproblems of moving a stack of size *N âˆ’ 1*.\n",
    "\n",
    "Consider a stack of *N* disks that we wish to move from Tower 1 to Tower 3, and let *my_tower(N)* move a stack of size *N* to the desired tower (i.e., display the moves). How to write *my_tower* may not immediately be clear. However, if we think about the problem in terms of subproblems, we can see that we need to move the top *N-1* disks to the middle tower, then the bottom disk to the right tower, and then the *N-1* disks on the middle tower to the right tower. *my_tower* can display the instruction to move disk *N*, and then make recursive calls to *my_tower(N-1)* to handle moving the smaller towers. The calls to *my_tower(N-1)* make recursive calls to *my_tower(N-2)* and so on. A breakdown of the three steps is depicted in the following figure.\n",
    "\n",
    "\n",
    "<img src=\"images/06.02.02-Break_down.png\" alt=\"breakdown\" title=\"Breakdown of one iteration of the recursive solution of the Towers of Hanoi problem.\" width=\"400\"/>\n",
    "\n",
    "Following is a recursive solution to the Towers of Hanoi problem. Notice its compactness and simplicity. The code exactly reflects our intuition about the recursive nature of the solution: First we move a stack of size *N-1* from the original tower 'from_tower' to the alternative tower 'alt_tower'. This is a difficult task, so instead we make a recursive call that will make subsequent recursive calls, but will, in the end, move the stack as desired. Then we move the bottom disk to the target tower 'to_tower'. Finally, we move the stack of size *N-1* to the target tower by making another recursive call.\n",
    "\n",
    "**TRY IT!** Use the function *my_towers* to solve the Towers of Hanoi Problem for *N = 3*. Verify the solution is correct by inspection. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "button": false,
    "collapsed": true,
    "jupyter": {
     "outputs_hidden": true
    },
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "def my_towers(N, from_tower, to_tower, alt_tower):\n",
    "    \"\"\"\n",
    "    Displays the moves required to move a tower of size N from the\n",
    "    'from_tower' to the 'to_tower'. \n",
    "    \n",
    "    'from_tower', 'to_tower' and 'alt_tower' are uniquely either \n",
    "    1, 2, or 3 referring to tower 1, tower 2, and tower 3. \n",
    "    \"\"\"\n",
    "    \n",
    "    if N != 0:\n",
    "        # recursive call that moves N-1 stack from starting tower\n",
    "        # to alternate tower\n",
    "        my_towers(N-1, from_tower, alt_tower, to_tower)\n",
    "        \n",
    "        # display to screen movement of bottom disk from starting\n",
    "        # tower to final tower\n",
    "        print(\"Move disk %d from tower %d to tower %d.\"\\\n",
    "                  %(N, from_tower, to_tower))\n",
    "        \n",
    "        # recursive call that moves N-1 stack from alternate tower\n",
    "        # to final tower\n",
    "        my_towers(N-1, alt_tower, to_tower, from_tower)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Move disk 1 from tower 1 to tower 3.\n",
      "Move disk 2 from tower 1 to tower 2.\n",
      "Move disk 1 from tower 3 to tower 2.\n",
      "Move disk 3 from tower 1 to tower 3.\n",
      "Move disk 1 from tower 2 to tower 1.\n",
      "Move disk 2 from tower 2 to tower 3.\n",
      "Move disk 1 from tower 1 to tower 3.\n"
     ]
    }
   ],
   "source": [
    "my_towers(3, 1, 3, 2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "source": [
    "By using Divide and Conquer, we have solved the Towers of Hanoi problem by making recursive calls to slightly smaller Towers of Hanoi problems that, in turn, make recursive calls to yet smaller Towers of Hanoi problems. Together, the solutions form the solution to the whole problem. The actual work done by a single function call is actually quite small: two recursive calls and moving one disk. In other words, a function call does very little work (moving a disk), and then passes the rest of the work onto other calls, a skill you will probably find very useful throughout your engineering career."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "source": [
    "## Quicksort\n",
    "\n",
    "An list of numbers, *A*, is **sorted** if the elements are arranged in ascending or descending order. Although there are many ways of sorting a list, *quicksort* is a divide-and-conquer approach that is a very fast algorithm for sorting using a single processor (there are faster algorithms for multiple processors).\n",
    "\n",
    "The *quicksort* algorithm starts with the observation that sorting a list is hard, but comparison is easy. So instead of sorting a list, we separate the list by comparing to a **pivot**. At each recursive call to *quicksort*, the input list is divided into three parts: elements that are smaller than the pivot, elements that are equal to the pivot, and elements that are larger than the pivot. Then a recursive call to *quicksort* is made on the two subproblems: the list of elements smaller than the pivot and the list of elements larger than the pivot. Eventually the subproblems are small enough (i.e., list size of length 1 or 0) that sorting the list is trivial.\n",
    "\n",
    "Consider the following recursive implementation of *quicksort*."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "button": false,
    "collapsed": true,
    "jupyter": {
     "outputs_hidden": true
    },
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "def my_quicksort(lst):\n",
    "    \n",
    "    if len(lst) <= 1:\n",
    "        # list of length 1 is easiest to sort \n",
    "        # because it is already sorted\n",
    "        \n",
    "        sorted_list = lst    \n",
    "    else:\n",
    "        \n",
    "        # select pivot as teh first element of the list\n",
    "        pivot = lst[0]\n",
    "        \n",
    "        # initialize lists for bigger and smaller elements \n",
    "        # as well those equal to the pivot\n",
    "        bigger = []\n",
    "        smaller = []\n",
    "        same = []\n",
    "        \n",
    "        # loop through list and put elements into appropriate array\n",
    "        \n",
    "        for item in lst:\n",
    "            if item > pivot:\n",
    "                bigger.append(item)\n",
    "            elif item < pivot:\n",
    "                smaller.append(item)\n",
    "            else:\n",
    "                same.append(item)\n",
    "        \n",
    "        sorted_list = my_quicksort(smaller) + same + my_quicksort(bigger)\n",
    "        \n",
    "    return sorted_list"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[1, 2, 3, 3, 5, 6, 8, 10]"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "my_quicksort([2, 1, 3, 5, 6, 3, 8, 10])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "source": [
    "Similarly to Towers of Hanoi, we have broken up the problem of sorting (hard) into many comparisons (easy). "
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
