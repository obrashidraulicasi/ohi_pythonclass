# Curso grado FICH-UNL: Obras Hidráulica I

Docentes: Felipe Franco, Lucas Dominguez Ruben y Sebastian Schmidt

- Programa de materia: [Programa](PROGRAMACION_2023-OHI.pdf)
- Webpage: 
- Clases:
 	-	Miercoles de Teoría 14-17hs Aula 6
 	-   Viernes Teoría-Práctica 15-19hs Aula 10
- [Página de gráficas](https://ldominguezruben.gitlab.io/obrashidraulicasi/)

aportes, contacto, aqui: ldominguez@fich.unl.edu.ar

## Generalidades
- Para regularizar
		- Presencia del 80% de las Clases Prácticas
		- Aprobar los TPs y Laboratorios
		- Aprobar ambos parciales teóricos-Practicos con mas 4 
- Para promocionar
		- Presencia del 80% de las Clases Prácticas
		- Aprobar los TPs y Laboratorios
		- Aprobar ambos parciales teóricos-Practicos con mas 7
		- Aprobar el Coloquioo Integrador


## Bibliografía
- [e-FICH Plataforma Educativa](http://e-fich.unl.edu.ar/moodle/entrar.php?errorcode=4)
- [Curso de Python para ciencias e ingenierías](https://github.com/mgaitan/curso-python-cientifico)

## Dependencias
- numpy
- scipy
- matplotlib
- plotly
- pandas


```python

```
